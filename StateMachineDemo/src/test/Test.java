/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import stateMachine.EventApiBuilder;
import stateMachine.StateApiBuilder;
import stateMachine.StateMachineApiBuilder;
import stateMachine.StateMachineController;
import stateMachine.StateMachineFileBuilder;
import stateMachine.TransitionApiBuilder;

/**
 *
 * @author maja
 */
public class Test {

    public static void main(String[] args) {
        StateMachineController stateMachineController;
//        stateMachineController = getViaApiBuilder();
        stateMachineController = getViaFileBuilder();
        stateMachineController.initializeStateMachine();
    }

    private static StateMachineController getViaApiBuilder() {
        StateApiBuilder stateApiBuilder = StateMachineApiBuilder.createStateApiBuilder();
        stateApiBuilder.addState(1, "Created");
        stateApiBuilder.addState(2, "Waiting");
        stateApiBuilder.addState(3, "Running");
        stateApiBuilder.addState(4, "Blocked");
        stateApiBuilder.addState(5, "Terminated");
        EventApiBuilder eventApiBuilder = stateApiBuilder.buildStates();
        eventApiBuilder.addEvent(1, "Wait");
        eventApiBuilder.addEvent(2, "Run");
        eventApiBuilder.addEvent(3, "Terminate");
        eventApiBuilder.addEvent(4, "Block");
        TransitionApiBuilder transitionApiBuilder = eventApiBuilder.buildEvents();
        transitionApiBuilder.addTransition(1, 1, 2);
        transitionApiBuilder.addTransition(2, 2, 3);
        transitionApiBuilder.addTransition(3, 1, 2);
        transitionApiBuilder.addTransition(3, 3, 5);
        transitionApiBuilder.addTransition(3, 4, 4);
        transitionApiBuilder.addTransition(4, 1, 2);
        return transitionApiBuilder.buildTransitions();
    }

    private static StateMachineController getViaFileBuilder() {
        StateMachineFileBuilder stateMachineFileBuilder = new StateMachineFileBuilder("states.csv", "events.csv", "transitions.csv");
        return stateMachineFileBuilder.buildStateMachine();
    }
    
}
