/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

/**
 *
 * @author maja
 */
public class TransitionApiBuilder {

    private static final String TRANSITION_BUILD_FINISHED = "Transitions are already built.";
    private static final String TRANSITION_EXISTS = "Target state for source state with id [%s], for event with id [%s], already exists with id [%s].";

    private final StateMachine stateMachine;
    private boolean finished;

    TransitionApiBuilder(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }

    public void addTransition(int sourceStateId, int eventId, int targetStateId) {
        if (!finished) {
            State sourceState = Utils.getStateById(stateMachine.getStates(), sourceStateId);
            Event event = Utils.getEventById(stateMachine.getEvents(), eventId);
            State targetState = Utils.getStateById(stateMachine.getStates(), targetStateId);
            State existingTargetState = sourceState.getTargetState(eventId);
            if (existingTargetState != null) {
                throw new StateMachineException(String.format(TRANSITION_EXISTS, sourceStateId, event.getEventId(), existingTargetState.getStateId()));
            }
            sourceState.addTargetState(event.getEventId(), targetState);
        } else {
            throw new StateMachineException(TRANSITION_BUILD_FINISHED);
        }
    }

    public StateMachineController buildTransitions() {
        if (!finished) {
            finished = true;
            return new StateMachineController(stateMachine);
        }
        throw new StateMachineException(TRANSITION_BUILD_FINISHED);
    }

}
