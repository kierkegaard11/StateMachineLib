/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class StateFileBuilder {

    private static final String STATE_BUILD_FINISHED = "States are already built.";
    private static final String STATES_FILE_READING_ERROR = "Error occured while reading states file.";
    private static final String STATE_EXISTS_ERROR = "State with id [%s] already exists.";
    private static final String STATES_EMPTY = "States cannot be empty.";

    private final StateMachine stateMachine;
    private final List<State> states;
    private final String statesFilePath;
    private final String eventsFilePath;
    private final String transitionsFilePath;
    private boolean finished;

    StateFileBuilder(String statesFilePath, String eventsFilePath, String transitionsFilePath) {
        this.statesFilePath = statesFilePath;
        this.eventsFilePath = eventsFilePath;
        this.transitionsFilePath = transitionsFilePath;
        stateMachine = new StateMachine();
        states = new ArrayList<>();
    }

    EventFileBuilder buildStates() {
        if (!finished) {
            loadStatesFromFile();
            if (states.isEmpty()) {
                throw new StateMachineException(STATES_EMPTY);
            }
            stateMachine.setStates(states);
            finished = true;
            return new EventFileBuilder(stateMachine, eventsFilePath, transitionsFilePath);
        }
        throw new StateMachineException(STATE_BUILD_FINISHED);
    }

    private void loadStatesFromFile() {
        BufferedReader csvReader = null;
        try {
            csvReader = new BufferedReader(new FileReader(statesFilePath));
            loadStates(csvReader);
        } catch (IOException | ArrayIndexOutOfBoundsException | NumberFormatException ex) {
            throw new StateMachineException(STATES_FILE_READING_ERROR);
        } finally {
            if (csvReader != null) {
                try {
                    csvReader.close();
                } catch (IOException ex) {
                    throw new StateMachineException(STATES_FILE_READING_ERROR);
                }
            }
        }
    }

    private void loadStates(BufferedReader csvReader) throws IOException {
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            int stateId = Integer.parseInt(data[0]);
            String name = data[1];
            if (!Utils.stateExists(states, stateId)) {
                states.add(new State(stateId, name));
            } else {
                throw new StateMachineException(String.format(STATE_EXISTS_ERROR, stateId));
            }
        }
    }

}
