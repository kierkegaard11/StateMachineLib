/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.awt.Graphics;
import java.awt.Point;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class StateMachinePanel extends JPanel {

    private StateMachine stateMachine;
    private List<StateLabel> stateLabels;

    StateMachinePanel(StateMachine stateMachine, List<StateLabel> stateLabels) {
        this.stateMachine = stateMachine;
        this.stateLabels = stateLabels;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        for (StateLabel stateLabel : stateLabels) {
            drawTransitions(stateLabel, g);
        }
    }
    
    private void drawTransitions(StateLabel sourceStateLabel, Graphics g) {
        Map<Integer, State> targetStates = sourceStateLabel.getState().getTargetStates();
        Set<Integer> eventIds = targetStates.keySet();
        for (int eventId : eventIds) {
            StateLabel targetStateLabel = getTargetStateLabel(targetStates.get(eventId));
            if (targetStateLabel != null) {
                Point sourceCenterPoint = sourceStateLabel.getCenterPoint();
                Point targetCenterPoint = targetStateLabel.getCenterPoint();
                g.drawLine(sourceCenterPoint.x, sourceCenterPoint.y, targetCenterPoint.x, targetCenterPoint.y);
            }
        }
    }

    private StateLabel getTargetStateLabel(State targetState) {
        for (StateLabel stateLabel : stateLabels) {
            if (stateLabel.getState().equals(targetState)) {
                return stateLabel;
            }
        }
        return null;
    }

}
