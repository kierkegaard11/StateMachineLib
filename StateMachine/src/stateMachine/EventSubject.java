/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class EventSubject {

    private static EventSubject instance;
    private final List<EventObserver> observers;

    private EventSubject() {
        observers = new ArrayList<>();
    }

    static EventSubject getInstance() {
        if (instance == null) {
            instance = new EventSubject();
        }
        return instance;
    }

    void subscribe(EventObserver observer) {
        observers.add(observer);
    }

    void notifyAllEventMouseEntered(Event event) {
        for (EventObserver observer : observers) {
            observer.notifyMouseEntered(event);
        }
    }

    void notifyAllEventMouseClicked(Event event) {
        for (EventObserver observer : observers) {
            observer.notifyMouseClicked(event);
        }
    }

    void notifyAllEventMouseExited(Event event) {
        for (EventObserver observer : observers) {
            observer.notifyMouseExited(event);
        }
    }

}
