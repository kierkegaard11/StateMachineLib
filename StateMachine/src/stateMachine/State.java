/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maja
 */
public class State {
    
    private final int stateId;
    private final String name;
    private final Map<Integer, State> targetStates;

    State(int stateId, String name) {
        this.stateId = stateId;
        this.name = name;
        targetStates = new HashMap<>();
    }

    int getStateId() {
        return stateId;
    }

    String getName() {
        return name;
    }

    void addTargetState(Integer eventId, State targetState) {
        targetStates.put(eventId, targetState);
    }

    State getTargetState(int eventId) {
        return targetStates.get(eventId);
    }
    
    Map<Integer, State> getTargetStates() {
        return targetStates;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.stateId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final State other = (State) obj;
        if (this.stateId != other.stateId) {
            return false;
        }
        return true;
    }
    
}
