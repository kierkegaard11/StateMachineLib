/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.List;

/**
 *
 * @author maja
 */
public class StateMachineController {

    private static final String STATE_NOT_FOUND = "State with id [%s] does not exist.";
    
    private final StateMachine stateMachine;
    private final StateMachineForm stateMachineForm;
    private final StateMachineFormDrawer stateMachineDrawer;
    private final EventFormController eventFormController;
    private boolean stateMachineInitialized;

    StateMachineController(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
        stateMachineForm = new StateMachineForm();
        eventFormController = new EventFormController(stateMachineForm, stateMachine.getEvents());
        stateMachineDrawer = new StateMachineFormDrawer(stateMachine, stateMachineForm, this);
    }

    public void setCurrentState(int currentStateId) {
        stateMachine.setCurrentState(getStateForStateId(currentStateId));
        enableDisableEvents();
    }

    private void enableDisableEvents() {
        State currentState = stateMachine.getCurrentState();
        for (EventButton eventButton : eventFormController.getEventButtons()) {
            int eventId = eventButton.getEvent().getEventId();
            if (currentState.getTargetState(eventId) != null) {
                eventButton.setEnabled(true);
            } else {
                eventButton.setEnabled(false);
            }
        }
    }

    private State getStateForStateId(int stateId) {
        List<State> states = stateMachine.getStates();
        for (State state : states) {
            if (state.getStateId() == stateId) {
                return state;
            }
        }
        return null;
    }

    public void initializeStateMachine() {
        internalInitializeState(null);
    }

    public void initializeStateMachine(int currentStateId) {
        if (!Utils.stateExists(stateMachine.getStates(), currentStateId)) {
            throw new StateMachineException(String.format(STATE_NOT_FOUND, currentStateId));
        }
        internalInitializeState(currentStateId);
    }

    private void internalInitializeState(Integer currentStateId) {
        if (!stateMachineInitialized) {
            eventFormController.drawEventForm();
            if (currentStateId != null) {
                setCurrentState(currentStateId);
            } else {
                setCurrentState(stateMachine.getStates().get(0).getStateId());
            }
            stateMachineDrawer.drawStateMachineForm();
            stateMachineForm.setLocationRelativeTo(null);
            stateMachineForm.setVisible(true);
            eventFormController.showEventForm();
            stateMachineInitialized = true;
        }
    }

}
