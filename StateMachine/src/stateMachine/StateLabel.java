/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

/**
 *
 * @author maja
 */
public class StateLabel extends JLabel implements EventObserver {

    static final Color STATE_INACTIVE_COLOR = Color.BLACK;
    static final Color STATE_TARGET_COLOR = Color.GREEN;
    static final Color STATE_ACTIVE_COLOR = Color.RED;
    static final int BORDER_THICKNESS = 3;

    private final StateMachineController stateMachineController;
    private final StateMachine stateMachine;
    private final State state;
    private final int x;
    private final int y;
    private final Point centerPoint;

    StateLabel(StateMachineController stateMachineController, StateMachine stateMachine, State state, int x, int y) {
        super(state.getName());
        this.stateMachineController = stateMachineController;
        this.stateMachine = stateMachine;
        this.state = state;
        this.x = x;
        this.y = y;
        this.centerPoint = getCenterPoint(x, y);
        super.setHorizontalAlignment(SwingConstants.CENTER);
        super.setOpaque(true);
        super.setBackground(Color.BLACK);
        super.setForeground(Color.WHITE);
        EventSubject.getInstance().subscribe(this);
    }

    State getState() {
        return state;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    Point getCenterPoint() {
        return centerPoint;
    }

    void setBorder(Color borderColor) {
        super.setBorder(new LineBorder(borderColor, BORDER_THICKNESS));
    }

    @Override
    public void notifyMouseEntered(Event event) {
        if (isTargetState(event)) {
            setBorder(STATE_TARGET_COLOR);
        }
    }

    @Override
    public void notifyMouseExited(Event event) {
        if (isTargetState(event)) {
            setBorder(STATE_INACTIVE_COLOR);
            if (state.equals(stateMachine.getCurrentState())) {
                setBorder(STATE_ACTIVE_COLOR);
            }
        }
    }

    @Override
    public boolean notifyMouseClicked(Event event) {
        if (isTargetState(event)) {
            stateMachineController.setCurrentState(state.getStateId());
            setBorder(STATE_ACTIVE_COLOR);
            return true;
        }
        setBorder(STATE_INACTIVE_COLOR);
        return false;
    }

    private boolean isTargetState(Event event) {
        State currentState = stateMachine.getCurrentState();
        State targetState = currentState.getTargetState(event.getEventId());
        return targetState != null && state.getStateId() == targetState.getStateId();
    }

    private Point getCenterPoint(int x, int y) {
        x += StateMachineFormDrawer.STATE_LABEL_WIDTH / 2;
        y += StateMachineFormDrawer.STATE_LABEL_HEIGHT / 2;
        return new Point(x, y);
    }

}
