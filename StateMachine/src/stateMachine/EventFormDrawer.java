/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class EventFormDrawer {

    private static final int NUMBER_OF_COLUMNS = 2;
    private static final int VERTICAL_GAP = 5;
    private static final int HORIZONTAL_GAP = 5;

    private static final int EVENT_BUTTON_WIDTH = 150;
    private static final int EVENT_BUTTON_HEIGHT = 50;

    private final EventForm eventForm;
    private final List<Event> events;
    private final List<EventButton> eventButtons;

    EventFormDrawer(EventForm eventForm, List<Event> events) {
        this.eventForm = eventForm;
        this.events = events;
        eventButtons = new ArrayList<>();
    }

    void drawEventForm() {
        adjustEventForm();
        addEventsToEventForm();
    }

    private void adjustEventForm() {
        int numberOfRows = (int) Math.round((double) events.size() / NUMBER_OF_COLUMNS);
        GridLayout grid = (GridLayout) eventForm.getContentPane().getLayout();
        grid.setColumns(NUMBER_OF_COLUMNS);
        grid.setRows(numberOfRows);
        grid.setVgap(VERTICAL_GAP);
        grid.setHgap(HORIZONTAL_GAP);
        eventForm.setSize(getEventFormWidth(), getEventFormHeight(numberOfRows));
    }

    private int getEventFormWidth() {
        int width = 0;
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            width += EVENT_BUTTON_WIDTH + HORIZONTAL_GAP;
        }
        return width;
    }

    private int getEventFormHeight(int numberOfRows) {
        int height = 0;
        for (int i = 0; i < numberOfRows; i++) {
            height += EVENT_BUTTON_HEIGHT + VERTICAL_GAP;
        }
        return height;
    }

    private void addEventsToEventForm() {
        for (Event event : events) {
            EventButton eventButton = new EventButton(event);
            addMouseEnteredListener(eventButton);
            addMouseExitedListener(eventButton);
            addMouseClickedListener(eventButton);
            eventForm.add(eventButton);
            eventButtons.add(eventButton);
        }
    }

    private void addMouseEnteredListener(EventButton eventButton) {
        eventButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (eventButton.isEnabled()) {
                    super.mouseEntered(e);
                    EventSubject.getInstance().notifyAllEventMouseEntered(eventButton.getEvent());
                }
            }
        });
    }

    private void addMouseExitedListener(EventButton eventButton) {
        eventButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                if (eventButton.isEnabled()) {
                    super.mouseExited(e);
                    EventSubject.getInstance().notifyAllEventMouseExited(eventButton.getEvent());
                }
            }
        });
    }

    private void addMouseClickedListener(EventButton eventButton) {
        eventButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (eventButton.isEnabled()) {
                    super.mouseClicked(e);
                    EventSubject.getInstance().notifyAllEventMouseClicked(eventButton.getEvent());
                }
            }
        });
    }

    List<EventButton> getEventButtons() {
        return eventButtons;
    }

}
