/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author maja
 */
public class TransitionFileBuilder {

    private static final String TRANSITIONS_BUILD_FINISHED = "Transitions are already built.";
    private static final String TRANSITIONS_FILE_READING_ERROR = "Error occured while reading transitions file.";
    private static final String TRANSITION_EXISTS = "Target state for source state with id [%s], for event with id [%s], already exists with id [%s].";

    private final StateMachine stateMachine;
    private final String transitionsFilePath;
    private boolean finished;

    TransitionFileBuilder(StateMachine stateMachine, String transitionsFilePath) {
        this.stateMachine = stateMachine;
        this.transitionsFilePath = transitionsFilePath;
    }

    StateMachineController buildTransitions() {
        if (!finished) {
            loadTransitionsFromFile();
            finished = true;
            return new StateMachineController(stateMachine);
        }
        throw new StateMachineException(TRANSITIONS_BUILD_FINISHED);
    }

    private void loadTransitionsFromFile() {
        BufferedReader csvReader = null;
        try {
            csvReader = new BufferedReader(new FileReader(transitionsFilePath));
            loadTransitions(csvReader);
        } catch (IOException | ArrayIndexOutOfBoundsException | NumberFormatException ex) {
            throw new StateMachineException(TRANSITIONS_FILE_READING_ERROR);
        } finally {
            if (csvReader != null) {
                try {
                    csvReader.close();
                } catch (IOException ex) {
                    throw new StateMachineException(TRANSITIONS_FILE_READING_ERROR);
                }
            }
        }
    }

    private void loadTransitions(BufferedReader csvReader) throws IOException {
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            int sourceStateId = Integer.parseInt(data[0]);
            int eventId = Integer.parseInt(data[1]);
            int targetStateId = Integer.parseInt(data[2]);
            State sourceState = Utils.getStateById(stateMachine.getStates(), sourceStateId);
            Event event = Utils.getEventById(stateMachine.getEvents(), eventId);
            State targetState = Utils.getStateById(stateMachine.getStates(), targetStateId);
            State existingTargetState = sourceState.getTargetState(eventId);
            if (existingTargetState != null) {
                throw new StateMachineException(String.format(TRANSITION_EXISTS, sourceStateId, event.getEventId(), existingTargetState.getStateId()));
            }
            sourceState.addTargetState(event.getEventId(), targetState);
        }
    }

}
