/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import org.netbeans.lib.awtextra.AbsoluteConstraints;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/**
 *
 * @author maja
 */
public class StateMachineFormDrawer {

    private static final int NUMBER_OF_COLUMNS = 3;
    private static final int VERTICAL_GAP = 150;
    private static final int HORIZONTAL_GAP = 150;

    static final int STATE_LABEL_WIDTH = 150;
    static final int STATE_LABEL_HEIGHT = 50;

    private final StateMachine stateMachine;
    private final StateMachineForm stateMachineForm;
    private final StateMachineController stateMachineController;
    private final List<StateLabel> stateLabels;

    StateMachineFormDrawer(StateMachine stateMachine, StateMachineForm stateMachineForm,
            StateMachineController stateMachineController) {
        this.stateMachine = stateMachine;
        this.stateMachineForm = stateMachineForm;
        this.stateMachineController = stateMachineController;
        stateLabels = new ArrayList<>();
    }

    void drawStateMachineForm() {
        adjustStateMachineForm();
        populateStateLabels();
        drawStatesOnStateMachineForm();
        highlightActiveState();
    }

    private void adjustStateMachineForm() {
        int width = getWidth();
        int height = getHeight();
        stateMachineForm.setSize(width, height);
    }

    private int getWidth() {
        int width = 0;
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            width += HORIZONTAL_GAP + STATE_LABEL_WIDTH;
        }
        width += HORIZONTAL_GAP;
        return width;
    }

    private int getHeight() {
        int height = 0;
        int numberOfRows = getNumberOfRows();
        for (int i = 0; i <= numberOfRows; i++) {
            height += STATE_LABEL_HEIGHT + VERTICAL_GAP;
        }
        return height;
    }

    private int getNumberOfRows() {
        return (int) Math.round((double) stateMachine.getStates().size() / NUMBER_OF_COLUMNS);
    }

    private void populateStateLabels() {
        int x = HORIZONTAL_GAP;
        int y = VERTICAL_GAP;
        List<State> states = stateMachine.getStates();
        for (int i = 1; i <= states.size(); i++) {
            StateLabel stateLabel = new StateLabel(stateMachineController, stateMachine, states.get(i - 1), x, y);
            stateLabels.add(stateLabel);
            x += STATE_LABEL_WIDTH + HORIZONTAL_GAP;
            if (i % 2 == 0) {
                y += VERTICAL_GAP / 2;
            }
            if (i % NUMBER_OF_COLUMNS == 0) {
                x = HORIZONTAL_GAP;
                y += STATE_LABEL_HEIGHT + VERTICAL_GAP;
            }
        }
    }

    private void highlightActiveState() {
        for (Component component : stateMachineForm.getContentPane().getComponents()) {
            if (component instanceof StateLabel) {
                StateLabel stateLabel = (StateLabel) component;
                if (stateLabel.getState().equals(stateMachine.getCurrentState())) {
                    stateLabel.setBorder(StateLabel.STATE_ACTIVE_COLOR);
                } else {
                    stateLabel.setBorder(StateLabel.STATE_INACTIVE_COLOR);
                }
            }
        }
    }

    private void drawStatesOnStateMachineForm() {
        JPanel contentPane = new StateMachinePanel(stateMachine, stateLabels);
        contentPane.setSize(stateMachineForm.getWidth(), stateMachineForm.getHeight());
        contentPane.setLayout(new AbsoluteLayout());
        for (StateLabel stateLabel : stateLabels) {
            contentPane.add(stateLabel, new AbsoluteConstraints(stateLabel.getX(), stateLabel.getY(), StateMachineFormDrawer.STATE_LABEL_WIDTH, StateMachineFormDrawer.STATE_LABEL_HEIGHT));
            contentPane.repaint();
        }
        contentPane.repaint();
        stateMachineForm.setContentPane(contentPane);
    }

}
