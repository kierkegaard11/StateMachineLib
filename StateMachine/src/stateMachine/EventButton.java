/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import javax.swing.JButton;

/**
 *
 * @author maja
 */
public class EventButton extends JButton {
    
    private final Event event;
    
    EventButton(Event event) {
        super(event.getName());
        this.event = event;
    }

    Event getEvent() {
        return event;
    }
    
}
