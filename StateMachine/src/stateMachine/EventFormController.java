/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.List;

/**
 *
 * @author maja
 */
public class EventFormController {
    
    private final EventForm eventForm;
    private final List<Event> events;
    private final EventFormDrawer eventFormDrawer;
    
    EventFormController(StateMachineForm stateMachineForm, List<Event> events) {
        this.events = events;
        eventForm = new EventForm(stateMachineForm, false);
        eventFormDrawer = new EventFormDrawer(eventForm, events);
    }
    
    void drawEventForm() {
        eventFormDrawer.drawEventForm();
    }
    
    void showEventForm() {
        eventForm.setLocationRelativeTo(null);
        eventForm.setVisible(true);
    }

    List<EventButton> getEventButtons() {
        return eventFormDrawer.getEventButtons();
    }
    
}
