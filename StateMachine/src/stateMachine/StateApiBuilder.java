/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class StateApiBuilder {

    private static final String STATE_EXISTS_ERROR = "State with id [%s] already exists.";
    private static final String STATE_BUILD_FINISHED = "States are already built.";
    private static final String STATES_EMPTY = "States cannot be empty.";

    private final StateMachine stateMachine;
    private final List<State> states;
    private boolean finished;

    StateApiBuilder(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
        states = new ArrayList<>();
    }

    public void addState(int stateId, String name) {
        if (!finished) {
            if (!Utils.stateExists(states, stateId)) {
                states.add(new State(stateId, name));
            } else {
                throw new StateMachineException(String.format(STATE_EXISTS_ERROR, stateId));
            }
        } else {
            throw new StateMachineException(STATE_BUILD_FINISHED);
        }
    }

    public EventApiBuilder buildStates() {
        if (!finished) {
            if (states.isEmpty()) {
                throw new StateMachineException(STATES_EMPTY);
            }
            stateMachine.setStates(states);
            finished = true;
            return new EventApiBuilder(stateMachine);
        }
        throw new StateMachineException(STATE_BUILD_FINISHED);
    }

}
