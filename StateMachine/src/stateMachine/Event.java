/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

/**
 *
 * @author maja
 */
public class Event {
    
    private final int eventId;
    private final String name;

    Event(int eventId, String name) {
        this.eventId = eventId;
        this.name = name;
    }

    int getEventId() {
        return eventId;
    }

    String getName() {
        return name;
    }

}
