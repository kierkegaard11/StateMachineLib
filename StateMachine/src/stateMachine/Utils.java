/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.List;

/**
 *
 * @author maja
 */
public class Utils {

    private static final String STATE_NOT_FOUND = "State with id [%s] does not exist.";
    private static final String EVENT_NOT_FOUND = "State with id [%s] does not exist.";

    static State getStateById(List<State> states, int stateId) {
        for (State state : states) {
            if (state.getStateId() == stateId) {
                return state;
            }
        }
        throw new StateMachineException(String.format(STATE_NOT_FOUND, stateId));
    }

    static boolean stateExists(List<State> states, int stateId) {
        for (State state : states) {
            if (state.getStateId() == stateId) {
                return true;
            }
        }
        return false;
    }
    
    static Event getEventById(List<Event> events, int eventId) {
        for (Event event : events) {
            if (event.getEventId() == eventId) {
                return event;
            }
        }
        throw new StateMachineException(String.format(EVENT_NOT_FOUND, eventId));
    }

    static boolean eventExists(List<Event> events, int eventId) {
        for (Event event : events) {
            if (event.getEventId() == eventId) {
                return true;
            }
        }
        return false;
    }

}
