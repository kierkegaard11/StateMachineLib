/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.io.File;

/**
 *
 * @author maja
 */
public class StateMachineFileBuilder {
    
    private static final String STATES_FILE_PATH_NULL = "States file path cannot be null.";
    private static final String STATES_FILE_PATH_EMPTY = "States file path cannot be empty.";
    
    private static final String EVENTS_FILE_PATH_NULL = "Events file path cannot be null.";
    private static final String EVENTS_FILE_PATH_EMPTY = "Events file path cannot be empty.";
    
    private static final String TRANSITIONS_FILE_PATH_NULL = "Transitions file path cannot be null.";
    private static final String TRANSITIONS_FILE_PATH_EMPTY = "Transitions file path cannot be empty.";
    
    private static final String FILE_NOT_FOUND = "File on the path [%s] does not exist.";
    
    private String statesFilePath;
    private String eventsFilePath;
    private String transitionsFilePath;

    public StateMachineFileBuilder(String statesFilePath, String eventsFilePath, String transitionsFilePath) {
        setStatesFilePath(statesFilePath);
        setEventsFilePath(eventsFilePath);
        setTransitionsFilePath(transitionsFilePath);
    }
    
    public StateMachineController buildStateMachine() {
        StateFileBuilder stateFileBuilder = new StateFileBuilder(statesFilePath, eventsFilePath, transitionsFilePath);
        EventFileBuilder eventFieBuilder = stateFileBuilder.buildStates();
        TransitionFileBuilder transitionFileBuilder = eventFieBuilder.buildEvents();
        return transitionFileBuilder.buildTransitions();
    }
    
    private void setStatesFilePath(String statesFilePath) {
        if (statesFilePath == null) {
            throw new StateMachineException(STATES_FILE_PATH_NULL);
        }
        if (statesFilePath.isEmpty()) {
            throw new StateMachineException(STATES_FILE_PATH_EMPTY);
        }
        if (!new File(statesFilePath).exists()) {
            throw new StateMachineException(String.format(FILE_NOT_FOUND, statesFilePath));
        }
        this.statesFilePath = statesFilePath;
    }
    
    private void setEventsFilePath(String eventsFilePath) {
        if (eventsFilePath == null) {
            throw new StateMachineException(EVENTS_FILE_PATH_NULL);
        }
        if (eventsFilePath.isEmpty()) {
            throw new StateMachineException(EVENTS_FILE_PATH_EMPTY);
        }
        if (!new File(eventsFilePath).exists()) {
            throw new StateMachineException(String.format(FILE_NOT_FOUND, statesFilePath));
        }
        this.eventsFilePath = eventsFilePath;
    }
    
    private void setTransitionsFilePath(String transitionsFilePath) {
        if (transitionsFilePath == null) {
            throw new StateMachineException(TRANSITIONS_FILE_PATH_NULL);
        }
        if (transitionsFilePath.isEmpty()) {
            throw new StateMachineException(TRANSITIONS_FILE_PATH_EMPTY);
        }
        if (!new File(transitionsFilePath).exists()) {
            throw new StateMachineException(String.format(FILE_NOT_FOUND, transitionsFilePath));
        }
        this.transitionsFilePath = transitionsFilePath;
    }
    
}
