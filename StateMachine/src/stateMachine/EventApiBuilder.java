/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class EventApiBuilder {

    private static final String EVENT_EXISTS_ERROR = "Event with id [%s] already exists.";
    private static final String EVENT_BUILD_FINISHED = "Events are already built.";
    private static final String EVENTS_EMPTY = "Events cannot be empty.";

    private final StateMachine stateMachine;
    private final List<Event> events;
    private boolean finished;

    EventApiBuilder(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
        events = new ArrayList<>();
    }

    public void addEvent(int eventId, String name) {
        if (!finished) {
            if (!Utils.eventExists(events, eventId)) {
                events.add(new Event(eventId, name));
            } else {
                throw new StateMachineException(String.format(EVENT_EXISTS_ERROR, eventId));
            }
        } else {
            throw new StateMachineException(EVENT_BUILD_FINISHED);
        }
    }

    public TransitionApiBuilder buildEvents() {
        if (!finished) {
            if (events.isEmpty()) {
                throw new StateMachineException(EVENTS_EMPTY);
            }
            stateMachine.setEvents(events);
            finished = true;
            return new TransitionApiBuilder(stateMachine);
        }
        throw new StateMachineException(EVENT_BUILD_FINISHED);
    }

}
