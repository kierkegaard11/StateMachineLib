/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class EventFileBuilder {

    private static final String EVENTS_BUILD_FINISHED = "Events are already built.";
    private static final String EVENTS_FILE_READING_ERROR = "Error occured while reading events file.";
    private static final String EVENT_EXISTS_ERROR = "Event with id [%s] already exists.";
    private static final String EVENTS_EMPTY = "Events cannot be empty.";

    private final StateMachine stateMachine;
    private final List<Event> events;
    private final String eventsFilePath;
    private final String transitionsFilePath;
    private boolean finished;

    EventFileBuilder(StateMachine stateMachine, String eventsFilePath, String transitionsFilePath) {
        this.eventsFilePath = eventsFilePath;
        this.transitionsFilePath = transitionsFilePath;
        this.stateMachine = stateMachine;
        events = new ArrayList<>();
    }

    TransitionFileBuilder buildEvents() {
        if (!finished) {
            loadEventsFromFile();
            if (events.isEmpty()) {
                throw new StateMachineException(EVENTS_EMPTY);
            }
            stateMachine.setEvents(events);
            finished = true;
            return new TransitionFileBuilder(stateMachine, transitionsFilePath);
        }
        throw new StateMachineException(EVENTS_BUILD_FINISHED);
    }

    private void loadEventsFromFile() {
        BufferedReader csvReader = null;
        try {
            csvReader = new BufferedReader(new FileReader(eventsFilePath));
            loadEvents(csvReader);
        } catch (IOException | ArrayIndexOutOfBoundsException | NumberFormatException ex) {
            throw new StateMachineException(EVENTS_FILE_READING_ERROR);
        } finally {
            if (csvReader != null) {
                try {
                    csvReader.close();
                } catch (IOException ex) {
                    throw new StateMachineException(EVENTS_FILE_READING_ERROR);
                }
            }
        }
    }

    private void loadEvents(BufferedReader csvReader) throws IOException {
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            int eventId = Integer.parseInt(data[0]);
            String name = data[1];
            if (!Utils.eventExists(events, eventId)) {
                events.add(new Event(eventId, name));
            } else {
                throw new StateMachineException(String.format(EVENT_EXISTS_ERROR, eventId));
            }
        }
    }

}
