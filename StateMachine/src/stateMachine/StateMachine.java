/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateMachine;

import java.util.List;

/**
 *
 * @author maja
 */
public class StateMachine {

    private State currentState;
    private List<State> states;
    private List<Event> events;

    public StateMachine() {
    }
    
    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    State getCurrentState() {
        return currentState;
    }
    
    List<State> getStates() {
        return states;
    }

    void setStates(List<State> states) {
        this.states = states;
    }

    List<Event> getEvents() {
        return events;
    }

    void setEvents(List<Event> events) {
        this.events = events;
    }
    
}
